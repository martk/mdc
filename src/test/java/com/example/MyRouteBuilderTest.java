package com.example;

import io.quarkus.artemis.test.ArtemisTestResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.apache.activemq.artemis.jms.client.ActiveMQQueueConnectionFactory;
import org.junit.jupiter.api.Test;

import javax.jms.*;

@QuarkusTest
@QuarkusTestResource(ArtemisTestResource.class)
public class MyRouteBuilderTest {

    ConnectionFactory connectionFactory = new ActiveMQQueueConnectionFactory();

    @Test
    public void waitNtest() {
        try(JMSContext context = connectionFactory.createContext(Session.AUTO_ACKNOWLEDGE)) {
            JMSProducer jmsProducer = context.createProducer();
            jmsProducer.send(new ActiveMQQueue("queue/transacted"), "testmessage");
        }
    }

}