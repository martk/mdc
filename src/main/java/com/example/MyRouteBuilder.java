package com.example;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;

public class MyRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        from("jms:queue/transacted?connectionFactory=#xaConnectionFactory")
                .routeId("transactedRoute")

                // remove this line and the routeId and breadcrumbId will be in the logs
                .transacted("PROPAGATION_REQUIRED")

                .log(LoggingLevel.INFO, "another message")
                .to("log:done?level=INFO");

    }
}